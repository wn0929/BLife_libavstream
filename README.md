

1 库简介

 

libavstream为安卓而生，开发者可以在自己的APP内通过简单的引用实现高效的音频、视频和音视频的流化传输。在传输协议方面，目前支持RTMP、RTSP协议的C/S广播模式和MPEG-TS流格式P2P模式；在编码格式方面，视频编码采用H.264，,音频采用AAC。在支持的设备类型方面，涵盖了搭载安卓系统的智能手机、平板电脑、智能电视、智能眼镜以及车载设备等移动终端。下面以BLife为例，详细阐述开发者在开发过程中如何正确的使用libavstream库。

 

2 开发环境

 

libavstream库与Android Studio开发环境完美适配，采用基于Gradle的构建方式发布于jcenter仓库中。

项目地址为：https://bintray.com/iavstream/maven/libavstream/

库地址：    http://jcenter.bintray.com/com/iavstream/libavstream/

目前为1.1.0版本，本项目包含四个文件分别为：

1) libavstream-1.1.0-javadoc.jar

2) libavstream-1.1.0-sources.jar

3) libavstream-1.1.0.aar

4) libavstream-1.1.0.pom

其中文件1)为libavstream库的javadoc文件，可以作为API文档使用。2)为源码文件包，包含了所有源代码。文件3)为我们开发中需要调用的aar库文件，在开发过程中通过gradle实现在线自动构建到我们的应用程序中。4)为项目信息描述文件。

如图1所示,在开发过程中我们在Gradle Scripts下面可以看到两个build.gradle文件，一个是Project的，一个是Module的。其中Project的build.gradle内容应设置如图中所示，主要是将远程仓库源设置为jcenter。

 

 

图1  BLife的Project gradle文件配置

 

如图2所示，另外一个是Module的build.gradle文件内容设置设下，最重要的是需要在dependencies中添加依赖项：com.iavstream:libavstream:1.1.0。在添加之后在构建阶段gradle即可从jcenter仓库中获取libavstream库文件完成我们应用程序的编译和链接等相关工作。

 

图2  BLife的Module gradle文件配置

 

3 使用权限

Android定义了一种权限方案来保护设备上的资源和功能，所有应用必须通过在AndroidManifest.xml文件中声明相关使用权限。作为开发者，由于libavstream库是以Service

的形式被用户调用，因此需要在开发者的应用AndroidManifest.xml文件中添加一下声明：

       <service

            android:name="com.iavstream.libavstream.CameraService"

            android:enabled="true" >

            <intent-filter>

                <action android:name="com.iavstream.libavstream.CameraService" />

            </intent-filter>

        </service>

声明完后，就可以在相应的源文件中使用CameraService所提供的的方法实现音视频直播传输了。

   至于CameraService所使用的相关权限，在libavstream中已经声明，开发者无需在应用中重复声明，libavstream使用的权限如下：

 

<manifest xmlns:android="http://schemas.android.com/apk/res/android"

    package="com.iavstream.libavstream">

    <uses-feature android:name="android.hardware.camera" />

    <uses-feature android:name="android.hardware.camera.autofocus" />

    <uses-permission android:name="android.permission.INTERNET" />

    <uses-permission android:name="android.permission.RECORD_AUDIO" />

    <uses-permission android:name="android.permission.CAMERA" />

 </manifest>

 

4  API调用

 

如上所述，我们通过调用CameraService即可以实现基于设备摄像头和麦克的音视频流化传输服务。实现该流程首先需要进行相关参数的配置工作，主要是对MediaParameter进行赋值。然后再定义ServiceConnection对象，在该对象中实现cameraService对象与开发者应用之间的交互。最后是定义connection()函数和disconnection()函数，实现流化传输服务的开启和关闭。

 

1) 配置MediaParameter

   

public class MediaParameter {

    //video

    public	int video_fps=12;      //1~15fps           default:15fps

    public	int video_width=640;   //1280x720,640x480, 320x240  default:640x480

    public	int video_height=480;        

    public	int video_camside=2;   //1: front 2:back   default:back  

    public	int video_bitrate=400000; //200kbps~500kbps   default:400kbps

    public	int video_quality=3;  // 1:high 2normal 3 low 

    	

    //audio

    public	int audio_samplerate=48000; //96000, 88200, 64000, 48000, 44100, 32000,

                                    //24000, 22050, 16000, 12000, 11025, 8000, 0

    public	int audio_channel=1;        //1,2

    public	int audio_format=1;         //1:s16

    public	int audio_bitrate=64000;    //1:64000bps

        

    //output format type

    public 	int  formattype=1;     //1:video and audio 2:only video 3:only audio        

public   int  streamtype=1;     //1:mpeg-ts 2:rtsp

 

    //mpegts_address

    public    String tsaddress="10.10.10.154";

public    String portname="6666";

 

    //rtsp address

    public    String ipaddress="115.28.82.205"; 

    public    String sdpfilename="miphone.sdp";

 

    boolean equals(MediaParameter mp);  // compare two parameters 

    String GetMpegTsAddress(int streamtype,String tsaddress,String portname);

} 

MediaParameter定义为一个类，包含了视频相关参数、音频相关参数、输出流的内容类型和传输类型以及目的地址等相关信息。用户的使用方法为示例如下：

 

import com.iavstream.libavstream.MediaParameter;

.......

 

MediaParameter mediaparameter= null;

 

mediaparameter.formattype=1; //音视频同时传输

 

.......

 

2) 定义ServiceConnection

 

import com.iavstream.libavstream.CameraInterface.OnJniStateChangeListener;

import com.iavstream.libavstream.CameraService;

import com.iavstream.libavstream.CameraTextureView;

import com.iavstream.libavstream.MediaParameter;

 

float previewRate = -1f;

SurfaceTexture mSurface;

private CameraService cameraService;

MediaParameter mediaparameter= null;

 

 

private ServiceConnection sc = new ServiceConnection() {

@Override

public void onServiceConnected(ComponentName name, IBinder service) {

//connect Service

cameraService = ((CameraService.MyBinder) (service)).getService();

if(cameraService != null)

{				

    mediaparameter=getMediaParameter();			  

    cameraService.prepare(mediaparameter);			  

    cameraService.startpreview(mSurface, previewRate);			  

	cameraService.getCameraInterface().setOnJniStateChangeListener(

           new OnJniStateChangeListener(){

		  public void OnJniStateChange(int state) {

				 String str = "no error";

				 switch (state)

				 {

				    case -1: str="分配上下文错误";break;

				    case -2: str="无法连接到服务器,请检查后重试";break;

				    case -3: str="无法连接到服务器,请检查后重试";break;	

				    case -4: str="视频编码出错";break;

				    case -5: str="音频编码出错";break;

				    case -6: str="网络质量差,传输中断";break;

				    case -7: str="网络质量差,传输中断";break;

				    case -8: str="网络质量差,传输中断";break;					 

				 }

				 DisplayToast("错误 :"+str);

				 cameraService.stop();

				 cameraService = null;

			}				  

		  });

	    }

	}

	

@Override

public void onServiceDisconnected(ComponentName name) {

		//disconnect Service

		cameraService.stop();

		cameraService = null;

}

};

 

在使用CameraService之前，需要开发者定义ServiceConnection对象并将其实例化，并且重载onServiceConnected和onServiceDisconnected函数，在这两个函数中完成开发者应用与CameraService的交互。下面就使用的方法分别说明：

cameraService.prepare(mediaparameter);

该方法用于实现传输参数的配置，mediaparameter为配置好的相关参数，返回值为void;

 

 

 

cameraService.startpreview(mSurface, previewRate);	

该方法用于启动音视频传输服务，mSurface为SurfaceTexture对象，用于指定视频预览窗口；返回值为void; previewRate为float变量，指定了预览视频窗口的宽高比。

 cameraService

      .getCameraInterface()

          .setOnJniStateChangeListener(

                 new OnJniStateChangeListener()

           {

                     public void OnJniStateChange(int state) 

                     {

                           String str = "no error";

			           switch (state)

			           {

				           case -1: str="分配上下文错误";break;

				           case -2: str="无法连接到服务器,请检查后重试";break;

				           case -3: str="无法连接到服务器,请检查后重试";break;

				           case -4: str="视频编码出错";break;

				           case -5: str="音频编码出错";break;

				           case -6: str="网络质量差,传输中断";break;

				           case -7: str="网络质量差,传输中断";break;

				           case -8: str="网络质量差,传输中断";break;			 

			            }

			              DisplayToast("error :"+str);

			              cameraService.stop();

			              cameraService = null;

                     }

          });

 

CameraService曝露了OnJniStateChangeListener接口，开发者可以根据流媒体引擎的工作状态参数state在应用中作出相应处理。定义完接口之后，cameraService .getCameraInterface()

.setOnJniStateChangeListener( OnJniStateChangeListener jnistate)来指定开发者自定义的OnJniStateChangeListener接口。如果出错，就是用cameraService.stop()停止音视频流化传输服务。

public void onServiceDisconnected(ComponentName name)； 

当Service服务停止连接时，使用cameraService.stop()停止音视频流化传输服务。

 

3) 定义connection()函数和disconnection()函数

 

private void connection() {

    Intent intent = new Intent("com.iavstream.libavstream.CameraService");

    getActivity().bindService(intent, sc, Context.BIND_AUTO_CREATE);

}



private void disconnection() {

if(sc != null){

	getActivity().unbindService(sc);

}		

}

 

定义connection()函数绑定"com.iavstream.libavstream.CameraService"服务，同时指定ServiceConnection对象为步骤2中开发者自定义的sc。最后定义了disconnection()来解绑我们的"com.iavstream.libavstream.CameraService"服务。至此，开发者可以使用connection()函数使用iAVStream所提供的的音视频流化技术，使用disconnection() 停止该服务。

 

 
