package com.iavstream.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;

public class WireLessState {
	
	  Context mContext = null;
	  public WireLessState(Context c)
      {
		  mContext=c;   	  
      }
	  public String checkNetworkInfo() {
		
		ConnectivityManager conMan = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		// mobile 3G Data Network
		State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();	
		// WIFI
		State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();	   
	    if(mobile.toString()=="CONNECTED")
	    {
	    	return "mobile";	    
	    }
	    else if(wifi.toString()=="CONNECTED")
	    {
	    	return "wlan";
	    }
	    else
	    {
	    	return "null";
	    }	
	}
	  
	public boolean checkNetwork() {
		boolean flag = false;
		ConnectivityManager connectivityManager = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getActiveNetworkInfo() != null) {
			flag = connectivityManager.getActiveNetworkInfo().isAvailable();// 如果得到的network可用
		} 
		else 
		{

		}
		return flag;
	}
}