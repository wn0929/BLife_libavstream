package com.iavstream.utility;


import android.app.Dialog;
import android.content.Context;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.iavstream.blife.R;

//import com.chase.policespy.R;

public class OkCancelDialog  {
	
	  Context mContext = null;
	  String mTitle,mContent;
	  Dialog mDialog;
	  
	  public OkCancelDialog(Context c,String title,String content)
      {  	  
		  mContext=c;
		  mTitle=title;
		  mContent=content;
		
		  mDialog= new Dialog(mContext,
  				android.R.style.Theme_Translucent_NoTitleBar);
		  mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		  mDialog.setContentView(R.layout.r_okcanceldialogview);
  		((TextView) mDialog.findViewById(R.id.dialog_title)).setText(title);
  		((TextView) mDialog.findViewById(R.id.dialog_message)).setText(content);
      }
	  
	  public void setOkClickListener(OnClickListener ck){
		  ((Button) mDialog.findViewById(R.id.ok))
			.setOnClickListener(ck);				
	  }
     public void setCancelClickListener(OnClickListener ck){
    	 ((Button) mDialog.findViewById(R.id.cancel))
 		.setOnClickListener(ck);
	  }
	  public void DialogShow()
	  {
		  mDialog.show();
	  }
	  public void DialogHide()
	  {
		  mDialog.hide();
	  }
	
}