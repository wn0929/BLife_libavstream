

package com.iavstream.blife.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.iavstream.blife.BLifeApplication;
import com.iavstream.blife.R;
import com.iavstream.utility.GetContactInfo;
import com.iavstream.utility.MsgStuff;
import com.iavstream.utility.OkCancelDialog;

import java.util.ArrayList;
import java.util.HashMap;



public class ShareFragment extends Fragment {

	
	 private MyCustomAdapter mAdapter;    
	 GetContactInfo mcontactinfo;
	 ListView mListView;
	 int  mContactCount,mSelectCount;
	 CheckBox mItemSelect,mAllSelect;
	 Button  mSendInvite;
	 TextView mSelectStatus;
	 Context mContext = null;
	 View mRootView;
	 private BLifeApplication mApplication;


	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		   mRootView = inflater.inflate(R.layout.sendsms,container,false);
		   super.onCreate(savedInstanceState);
		   mApplication = (BLifeApplication)getActivity().getApplication();
		   
	        mContext = getActivity();
	        //init surface
	    	mListView=(ListView)mRootView.findViewById(R.id.sms_number);
	    	mAllSelect=(CheckBox)mRootView.findViewById(R.id.sms_selectall);
	    	mSendInvite=(Button)mRootView.findViewById(R.id.sms_send);
	    	mSelectStatus=(TextView)mRootView.findViewById(R.id.sms_selectstatus);
	        //init  mAdapter
	        mAdapter = new MyCustomAdapter(mContext);        
	        mcontactinfo=new GetContactInfo(mContext);
	        mcontactinfo.getPhoneContacts();        
	        mAdapter.copyItem(mcontactinfo.GetContactName(),mcontactinfo.GetContactNumber());      
	        mListView.setAdapter(mAdapter); 
	        mContactCount=mAdapter.getCount();
	        mSelectCount=0;
	        //add onclick method      
	        mAllSelect.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(CompoundButton buttonview, boolean ischecked) {
				
					mAdapter.SelectAll(ischecked);				
				}    	
	        	
	        });
	        
	        mSendInvite.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0) {
					mAdapter.InviteSelected();					
				}			
	        	
	        });
	        
	        UpdataStatus(mContactCount,mSelectCount);
	        
		return mRootView ;
	}
	
	
	 private class MyCustomAdapter extends BaseAdapter {        
	       
	        private ArrayList<String> mName = new ArrayList<String>();
	        private ArrayList<String> mNumber = new ArrayList<String>();

	        private LayoutInflater mInflater;
	        private boolean[] checks;
		    private HashMap<Integer,Integer> Selected;
		    MsgStuff mMsgstuff;
		    Context mContext = null;

	        public MyCustomAdapter(Context c) {
	        	mContext=c;
	            mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            mMsgstuff=new MsgStuff(mContext,false);
	            Selected = new HashMap<Integer, Integer>();
	           
	        }     
	        
	        public void copyItem(ArrayList<String> listname,ArrayList<String> listnumber) {
	        	mName=listname;
	        	mNumber=listnumber;
	        	checks=new boolean[getCount()];
	            notifyDataSetChanged();
	        }    
	       
	        @Override
	        public int getCount() {
	            return mNumber.size();
	        }

	        @Override
	        public String getItem(int position) {
	            return mNumber.get(position);
	        }
	        
	        @Override
	        public long getItemId(int position) {
	            return position;
	        }

	        public void SelectAll(boolean select){
	        	
	        	for(int i=0;i<getCount();i++)
	        	{
	        		checks[i] = select;
	        	}        	
	        	notifyDataSetChanged();        	
	        }
	        
	        public void InviteSelected()
	        {
	        	int j=0; 
	        	final String rtspaddress;
				//final String sdpfile;
				final String sdpfile;
	        	
	        	
	        	if(mApplication.rtspEnabled){
	        		rtspaddress=mApplication.rtspAddress;
	        		sdpfile=mApplication.sdpFilename;
	        	}
	        	else
	        	{
	        		return;
	        	}
	        	
	        	Selected.clear();        	
	        	//get total selected
	        	for(int i=0;i<getCount();i++)
	        	{
	        		if(checks[i]==true){
	        			
	        			Selected.put(j++, i);
	        		}
	        	}
	        	
	        	final OkCancelDialog ok=new OkCancelDialog(mContext,"确定","确定向 "+mName.get(Selected.get(0))+"等"+Selected.size()+"人发出邀请吗?");
				ok.setOkClickListener(new OnClickListener() {
					@Override
	    			public void onClick(View v) {
						
						for(int k=0;k<Selected.size();k++)
    					{
    					   mMsgstuff.SendMsg(mNumber.get(Selected.get(k)),ShowMessage(mName.get(Selected.get(k)),"rtsp://"+rtspaddress+"/"+sdpfile));
    					   DisplayToast("向"+mName.get(Selected.get(k))+mNumber.get(Selected.get(k))+"等"+Selected.size()+"人发送了短信邀请");
    					}
						
	    			ok.DialogHide();
	    			
	    		}});
				
				ok.setCancelClickListener(new OnClickListener() {
					@Override
	    			public void onClick(View v) {   			
	    			ok.DialogHide();
	    			
	    		}});			
				ok.DialogShow();
	        }
	        
	        @Override
	        public View getView(int position, View convertView, ViewGroup parent) {
	            ViewHolder holder = null;          
	            if (convertView == null) {
	                holder = new ViewHolder();               
	                convertView = mInflater.inflate(R.layout.item, null);
	                holder.contact_number = (TextView)convertView.findViewById(R.id.contact_number);
	                holder.contact_name = (TextView)convertView.findViewById(R.id.contact_name);
	                holder.contact_select=(CheckBox)convertView.findViewById(R.id.contact_select);                
	                convertView.setTag(holder);              
	            } else {
	                holder = (ViewHolder)convertView.getTag();
	            }

	            final int pos  = position;
	            holder.contact_name.setText(mName.get(position));
	            holder.contact_number.setText(mNumber.get(position));           
	            holder.contact_select.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener()
				{
				@Override
				public void onCheckedChanged(CompoundButton buttonview, boolean ischecked) {				
					String temp;
					checks[pos] = ischecked;
					int i,j=0;				
					for(i=0;i<getCount();i++)
					{
						if(checks[i]==true)j++;				
					}
					mSelectCount=j;
					UpdataStatus(mContactCount,mSelectCount);			
				}       	
	        	
	        });
	            holder.contact_select.setChecked(checks[pos]);            
	            return convertView;
	        }
	    }
	    
	    public static class ViewHolder {
	        public TextView contact_name;
	        public TextView contact_number;
	        public CheckBox contact_select;
	    }    
	    
	    public void DisplayToast(String str)
	    {
	    	Toast toast = Toast.makeText(mContext, str,Toast.LENGTH_SHORT);
	    	toast.setGravity(Gravity.TOP, 0, 220);
	    	toast.show();
	    }
	   
	    public void UpdataStatus(int ContactCount,int SelectCount)
	    {
	    	if(mSelectStatus==null)
	    	{
	    		mSelectStatus=(TextView)mRootView.findViewById(R.id.sms_selectstatus);
	    	}
	    	mSelectStatus.setText("共"+ContactCount+"人,已选择"+SelectCount+"人");
	    	
	    }
	    
	   public String  ShowMessage(String to,String liveaddress){
		   
		   String str;

		   str=to+",Blive直播地址"+liveaddress;
		   
		   return str;
		   
	   }
}
