

package com.iavstream.blife.ui;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.iavstream.blife.BLifeApplication;
import com.iavstream.blife.R;
import com.iavstream.utility.MsgStuff;

import java.util.ArrayList;



public class PlayListFragment extends Fragment {
	
	private MyCustomAdapter mAdapter;
	 ListView mListView;
	 TextView mMsgNum;
	 Context mContext = null;
	 View mRootView;
	 MsgStuff mMsgstuff;
	 private BLifeApplication mApplication;
	    

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		   mRootView = inflater.inflate(R.layout.playlistcontainer,container,false);
		
		   super.onCreate(savedInstanceState);
		   mApplication = (BLifeApplication)getActivity().getApplication();
	       mContext = getActivity();

		   mListView=(ListView)mRootView.findViewById(R.id.sms_contentlist);
		   mMsgNum=(TextView)mRootView.findViewById(R.id.sms_msgnum);
	        //init  mAdapter
		   mAdapter = new MyCustomAdapter(mContext);
		   mMsgstuff=new MsgStuff(mContext,true);

		   mAdapter.copyItem(mMsgstuff.getAllRtspAddress());
		   mListView.setAdapter(mAdapter);
		   mMsgNum.setText("共"+mAdapter.getCount()+"个邀请");
	        
		   mContext.registerReceiver(SMSBroadcastReceiver, new IntentFilter(
	    	        "android.provider.Telephony.SMS_RECEIVED"));
	        
		   return mRootView ;
	}
	
	
	 private class MyCustomAdapter extends BaseAdapter {        
	       
	        private ArrayList<String> mFrom = new ArrayList<String>();
	        private ArrayList<String> mContent = new ArrayList<String>();
	        private LayoutInflater mInflater;
	       
	        Context mContext = null;

	        public MyCustomAdapter(Context c) {
	        	mContext=c;
	            mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	           
	        }     
	        
	        public void copyItem(ArrayList<String> content) {

	        	int j=0;
	        	if(content==null){

	        		return;
	        	}
	        	mFrom.clear();
	        	mContent.clear();
	        	
	        	for(int i=0;i<content.size();i+=4)
	        	{
	        		j=i/4;
	        		mFrom.add(content.get(j*4)+"   "+content.get(j*4+1)+"   "+content.get(j*4+3));
	        		mContent.add(content.get(j*4+2));	        		
	        		
	        	}
	        	
	            notifyDataSetChanged();

	        }    
	       
	        @Override
	        public int getCount() {

	            return mContent.size();
	        }

	        @Override
	        public String getItem(int position) {
	            return mContent.get(position);
	        }
	        
	        @Override
	        public long getItemId(int position) {
	            return position;
	        }
	       

	        
	        @Override
	        public View getView(int position, View convertView, ViewGroup parent) {
	        	
	            ViewHolder holder = null;          
	            if (convertView == null) {

	                holder = new ViewHolder();               
	                convertView = mInflater.inflate(R.layout.playlist, null);
	                holder.msg_from = (TextView)convertView.findViewById(R.id.msg_from);
	                holder.msg_content = (TextView)convertView.findViewById(R.id.msg_content);	                           
	                convertView.setTag(holder);              
	            } else {
	                holder = (ViewHolder)convertView.getTag();
	            }
	            
	            final int pos  = position;
	            holder.msg_from.setText(mFrom.get(position));
	            holder.msg_content.setText(mContent.get(position));           

	            
	            convertView.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View arg0) {
						//send msg
			        	final Dialog lDialog = new Dialog(mContext,
			    				android.R.style.Theme_Translucent_NoTitleBar);
			    		lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			    		lDialog.setContentView(R.layout.r_okcanceldialogview);
			    		((TextView) lDialog.findViewById(R.id.dialog_title)).setText("邀请");
			    		((TextView) lDialog.findViewById(R.id.dialog_message)).setText("确定向"+mFrom.get(pos)+"等人发送邀请吗？");
			    		//((Button) lDialog.findViewById(R.id.ok)).setText("Ok");
			    		((Button) lDialog.findViewById(R.id.ok))
			    				.setOnClickListener(new OnClickListener() {
									@Override
			    					public void onClick(View v) {
			    					mApplication.jumpEnabled=true;
			    					mApplication.FromName=mFrom.get(pos);
			    					mApplication.FromAddress=mContent.get(pos);	
			    					mApplication.mPager.setCurrentItem(0);
			    							    					
			    					lDialog.dismiss();
			    				}});
			    		((Button) lDialog.findViewById(R.id.cancel))
			    				.setOnClickListener(new OnClickListener() {
			    					@Override
			    					public void onClick(View v) {

			    						lDialog.dismiss();
			    					}
			    				});
			    		lDialog.show();
						
					}        	
	             });
	           

	            return convertView;
	        }

	    }
	    
	    public static class ViewHolder {
	        public TextView msg_from;
	        public TextView msg_content;	       
	    }    
	    
	    public void DisplayToast(String str)
	    {
	    	Toast toast = Toast.makeText(mContext, str,Toast.LENGTH_SHORT);
	    	toast.setGravity(Gravity.BOTTOM, 0, 220);
	    	toast.show();
	    }
	    
	    BroadcastReceiver SMSBroadcastReceiver = new BroadcastReceiver() {
			    @Override
			    public void onReceive(Context context, Intent intent) {
			    	   mAdapter.copyItem(mMsgstuff.getAllRtspAddress());   
			    	   mMsgNum.setText("共"+mAdapter.getCount()+"个邀请");
			    }
	       };
	       
	       @Override  
	       public void onDestroy() {

	           super.onDestroy();  
	       }
}
