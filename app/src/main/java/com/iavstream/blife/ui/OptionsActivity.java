

package com.iavstream.blife.ui;



import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

import com.iavstream.blife.BLifeApplication;
import com.iavstream.blife.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OptionsActivity extends PreferenceActivity {

	private BLifeApplication mApplication = null;

	protected void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		mApplication = (BLifeApplication) getApplication();
		addPreferencesFromResource(R.xml.preferences);

		final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		final Preference videoEnabled = findPreference("stream_video");
		final Preference audioEnabled = findPreference("stream_audio");
		final ListPreference audioBitrate = (ListPreference) findPreference("audio_bitrate");
		final ListPreference videocamside = (ListPreference) findPreference("cam_side");
		final ListPreference videoResolution = (ListPreference) findPreference("video_resolution");
		final ListPreference videoBitrate = (ListPreference) findPreference("video_bitrate");
		final ListPreference videoFramerate = (ListPreference) findPreference("video_framerate");
		final CheckBoxPreference mpegtsEnabled = (CheckBoxPreference) findPreference("mpeg_ts_enabled");
		final CheckBoxPreference rtspEnabled = (CheckBoxPreference) findPreference("rtsp_enabled");
		
		final Preference mpegtsAddress = findPreference("mpegts_address");
		final Preference rtspAddress = findPreference("rtsp_address");
		
		boolean videoState = settings.getBoolean("stream_video", true);
		videocamside.setEnabled(videoState);
		videoResolution.setEnabled(videoState);
		videoBitrate.setEnabled(videoState);
		videoFramerate.setEnabled(videoState);
		
		audioBitrate.setValue(String.valueOf(mApplication.audioBitrate));
		videocamside.setValue(String.valueOf(mApplication.videoCamSide));
		videoFramerate.setValue(String.valueOf(mApplication.videoQuality.framerate));
		videoBitrate.setValue(String.valueOf(mApplication.videoQuality.bitrate/1000));
		videoResolution.setValue(mApplication.videoQuality.resX+"x"+mApplication.videoQuality.resY);
		
		videoResolution.setSummary(getString(R.string.settings0)+" "+videoResolution.getValue()+"px");
		videoFramerate.setSummary(getString(R.string.settings1)+" "+videoFramerate.getValue()+"fps");
		videoBitrate.setSummary(getString(R.string.settings2)+" "+videoBitrate.getValue()+"kbps");
		audioBitrate.setSummary(getString(R.string.settings6)+" "+audioBitrate.getValue()+"kbps");

		audioBitrate.setEnabled(settings.getBoolean("stream_audio", false));

		mpegtsEnabled.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {

				boolean state = (Boolean)newValue;
				mpegtsEnabled.setEnabled(state);
				Editor editor = settings.edit();
				if (!state) {
					editor.putBoolean("mpeg_ts_enabled", false);
				} else {
					editor.putBoolean("mpeg_ts_enabled", false);
				}
				editor.commit();
				return true;
			}
		});

		videoResolution.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				Editor editor = settings.edit();
				Pattern pattern = Pattern.compile("([0-9]+)x([0-9]+)");
				Matcher matcher = pattern.matcher((String)newValue);
				matcher.find();
				editor.putInt("video_resX", Integer.parseInt(matcher.group(1)));
				editor.putInt("video_resY", Integer.parseInt(matcher.group(2)));
				editor.commit();
				videoResolution.setSummary(getString(R.string.settings0)+" "+(String)newValue+"px");
				return true;
			}
		});

		videoFramerate.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				videoFramerate.setSummary(getString(R.string.settings1)+" "+(String)newValue+"fps");
				return true;
			}
		});

		videoBitrate.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				videoBitrate.setSummary(getString(R.string.settings2)+" "+(String)newValue+"kbps");
				return true;
			}
		});

		videoEnabled.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				boolean state = (Boolean)newValue;
				videocamside.setEnabled(state);
				videoResolution.setEnabled(state);
				videoBitrate.setEnabled(state);
				videoFramerate.setEnabled(state);
				return true;
			}
		});
		
		audioBitrate.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				audioBitrate.setSummary(getString(R.string.settings6)+" "+(String)newValue+"kbps");
				return true;
			}
		});

		audioEnabled.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				boolean state = (Boolean)newValue;
				audioBitrate.setEnabled(state);
				return true;
			}
		});

	}

}
